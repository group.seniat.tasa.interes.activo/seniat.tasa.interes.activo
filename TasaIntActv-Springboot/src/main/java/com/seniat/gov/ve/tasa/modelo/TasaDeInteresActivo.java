package com.seniat.gov.ve.tasa.modelo;

import java.math.BigInteger;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "tasaitacts", schema="mt-2")
public class TasaDeInteresActivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column( name = "fechaorg")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date fechaorg;
	
	@Column( name = "fechahast")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date fechahast;
	
	@Column(name = "valorut", length = 10, scale = 1)
	private BigInteger valorut;
	
	public TasaDeInteresActivo() {
	
	}

	public TasaDeInteresActivo(Long id, Date fechaorg, Date fechahast, BigInteger valorut) {
		super();
		this.id = id;
		this.fechaorg = fechaorg;
		this.fechahast = fechahast;
		this.valorut = valorut;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaorg() {
		return fechaorg;
	}

	public void setFechaorg(Date fechaorg) {
		this.fechaorg = fechaorg;
	}

	public Date getFechahast() {
		return fechahast;
	}

	public void setFechahast(Date fechahast) {
		this.fechahast = fechahast;
	}

	public BigInteger getValorut() {
		return valorut;
	}

	public void setValorut(BigInteger valorut) {
		this.valorut = valorut;
	}
	
	
	
}
