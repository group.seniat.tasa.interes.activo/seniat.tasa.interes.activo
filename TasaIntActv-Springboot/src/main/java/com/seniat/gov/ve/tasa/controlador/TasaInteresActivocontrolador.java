package com.seniat.gov.ve.tasa.controlador;

//import java.util.HashMap;
import java.util.List;

//import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seniat.gov.ve.tasa.excepciones.ResourceNotFoundException;
import com.seniat.gov.ve.tasa.modelo.TasaDeInteresActivo;
import com.seniat.gov.ve.tasa.repositorio.TasaInteresActivoRepositorio;

public class TasaInteresActivocontrolador {

	@RestController
	@RequestMapping("/api/v1/")
	@CrossOrigin(origins = "http://localhost:4200")
	public class TasaInteresActivoControlador {
		
		@Autowired
	    public TasaInteresActivoRepositorio repositorio;

	    // Metodo para listar todas las listarTodasLasTasaInteres
	    @GetMapping("/tasaitacts/")
	    public List<TasaDeInteresActivo> obtenerListaDeTasasInteres() {
	        return repositorio.findAll();
	    }

		//este metodo sirve para guardar el TasaDeInteresActivo
	@PostMapping("/tasaitacts")
	public TasaDeInteresActivo crearTasaInteresActivo(@RequestBody TasaDeInteresActivo tasaitacts) {
		return repositorio.save(tasaitacts);
	}
    
	//este metodo sirve para buscar un TasaDeInteresActivo
	@GetMapping("/tasaitacts/{id}")
	public ResponseEntity<TasaDeInteresActivo> obtenerTasaInteresActivoPorId(@PathVariable Long id) {
		TasaDeInteresActivo tasaitacts = repositorio.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("No existe la tasa con el ID: " + id));
	    return ResponseEntity.ok(tasaitacts);
	}
	
	//este metodo sirve para actualizar TasaDeInteresActivo
	@PutMapping("/tasaitacts/{id}")
	public ResponseEntity<TasaDeInteresActivo> actualizarTasaInteresActivo(@PathVariable Long id,@RequestBody TasaDeInteresActivo detallesTasaDeInteresActivo){
		TasaDeInteresActivo tasaitacts = repositorio.findById(id)
				            .orElseThrow(() -> new ResourceNotFoundException("No existe la tasa con el ID : " + id));
		
		tasaitacts.setId(detallesTasaDeInteresActivo.getId());
		tasaitacts.setFechaorg(detallesTasaDeInteresActivo.getFechaorg());
		tasaitacts.setFechahast(detallesTasaDeInteresActivo.getFechahast());
		tasaitacts.setValorut(detallesTasaDeInteresActivo.getValorut());
		
		TasaDeInteresActivo tasaitactsActualizado = repositorio.save(tasaitacts);
		return ResponseEntity.ok(tasaitactsActualizado);
    }
	
	//este metodo sirve para eliminar un TasaDeInteresActivo
	
	//@DeleteMapping("/tasaitacts/{id}")
	
	//cambio 
	//@DeleteMapping("/tasai/{id}")
//	public ResponseEntity<?> eliminarTasaInteresActivo(@PathVariable Long id) {
	//    Tasa tasa = repositorioTasai.findById(id)
	  //          .orElseThrow(() -> new ResourceNotFoundException("No existe la tasa con el ID: " + id));

	    // Validaciones adicionales (opcional)
	    // ...

	    //repositorioTasai.delete(tasa);

	    //return ResponseEntity.noContent().build();
	//}
	}
}
