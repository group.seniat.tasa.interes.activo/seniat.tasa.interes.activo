package com.seniat.gov.ve.tasa.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.seniat.gov.ve.tasa.modelo.TasaDeInteresActivo;

@Repository
public interface TasaInteresActivoRepositorio extends JpaRepository<TasaDeInteresActivo, Long>{

}
