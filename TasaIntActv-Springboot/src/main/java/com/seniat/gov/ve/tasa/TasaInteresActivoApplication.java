package com.seniat.gov.ve.tasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TasaInteresActivoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TasaInteresActivoApplication.class, args);
	}

}
