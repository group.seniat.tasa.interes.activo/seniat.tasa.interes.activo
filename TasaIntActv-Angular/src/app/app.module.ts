import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActualizarComponent } from './menu-dashboard/components/actualizar/actualizar.component';
import { EliminarComponent } from './menu-dashboard/components/eliminar/eliminar.component';
import { CrearComponent } from './menu-dashboard/components/crear/crear.component';

import { MenuDashboardComponent } from './menu-dashboard/menu-dashboard.component';
import { ListaTasaInteresActivosComponent } from './menu-dashboard/components/lista/lista-tasa-interes-activos/lista-tasa-interes-activos.component';
import { ConsultasPorIdComponent } from './menu-dashboard/components/consultas-por-id/consultas-por-id.component';
// import { FiltroPipe } from './components/consultar/filtro/filtro.component';



@NgModule({
  declarations: [
    AppComponent,
    ActualizarComponent,
    EliminarComponent,
    CrearComponent,
    MenuDashboardComponent,
    ListaTasaInteresActivosComponent,
    ConsultasPorIdComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


//Agrege el FormsModule