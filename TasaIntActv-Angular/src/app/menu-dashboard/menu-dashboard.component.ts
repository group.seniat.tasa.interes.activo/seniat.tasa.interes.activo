import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-menu-dashboard',
  templateUrl: './menu-dashboard.component.html',
  styleUrls: ['./menu-dashboard.component.css']
})
export class MenuDashboardComponent implements OnInit{

  rutaImagen: string = './assets/imagen/cintillo.png';

  mostrarComponente: string = ''; // Valor inicial para mostrar el componente "Consultar"


  constructor (private router: Router) { }

  ngOnInit(): void {    
  }


  mostrarLista() {
    this.mostrarComponente = 'tasa-interes';
  }

  mostrarCrear() {
    this.mostrarComponente = 'crear';
  }
}
