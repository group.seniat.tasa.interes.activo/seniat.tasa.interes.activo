

import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TasaI } from '../modelo/tasa-i';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private baseURL = "http://localhost:8080/api/v1/tasaitacts";
 
  // Esta Url obtine el listado de las tasas de interes activo
  constructor(private httpClient : HttpClient) { }

  // Este metodo sirve para obtener las tasas de interes activo
  obtenerListaDeTasasInteres(): Observable<TasaI[]> {
    return this.httpClient.get<TasaI[]>(`${this.baseURL}`);
  }
// Este metodo sirve para guardar tasas de interes activo
  crearTasaInteresActivo(tasaitact: TasaI): Observable<Object> {
    return this.httpClient.post(`${this.baseURL}`,tasaitact);
  }

  // Este metodo sirve para actualizar tasas de interes activo

  actualizarTasaInteresActivo(id: number, tasaitact: TasaI): Observable<Object> {
    return this.httpClient.put(`${this.baseURL}/${id}`,tasaitact);
  }

  // Este metodo sirve para obtener una tasa de interes activo por su ID

  obtenerTasaInteresActivoPorId(id: number): Observable<TasaI> {
    return this.httpClient.get<TasaI>(`${this.baseURL}/${id}`);
  }

  // // Este metodo sirve para eliminar una tasa de interes activo

  eliminarTasaInteresActivo(id: number): Observable<Object> {
    return this.httpClient.delete(`${this.baseURL}/${id}`);
  }
}