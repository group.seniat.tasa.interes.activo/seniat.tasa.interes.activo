
import { Component, OnInit} from '@angular/core';


import { TasaI } from '../../modelo/tasa-i';
import { GeneralService } from '../../lista_de_servicio/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';

@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit{

  tasaitact: TasaI = new TasaI();
  id: number; 
  fechaorg: Date;
  fechahast: Date;
  valorut: number;

  title = 'Eliminar La Tasa De Interes Activo';

  constructor(private generalService: GeneralService, private router: Router,
              private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit() {}

  onSubmit() {
    this.generalService.eliminarTasaInteresActivo(this.id).subscribe(() => {
      swal('Tasa eliminada', `La tasa con ID ${this.id} ha sido eliminada con éxito`, 'success');
      this.irAlaListaDeTasasInteres();
    }, error => {
      swal('Error', error.message, 'error');
    });
  }

  irAlaListaDeTasasInteres() {
    this.router.navigate(['/tasa-interes']);
  }
}