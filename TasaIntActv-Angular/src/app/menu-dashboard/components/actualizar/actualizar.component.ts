import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from '../../lista_de_servicio/general.service';
import { TasaI } from '../../modelo/tasa-i';
import swal from 'sweetalert';


@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.component.html',
  styleUrls: ['./actualizar.component.css']
})
export class ActualizarComponent implements OnInit {
  tasaitact: TasaI = new TasaI(); // Variable para la tasa que se actualizará
  id: number;
  constructor(private generalService: GeneralService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
      this.id = this.route.snapshot.params['id'];
      this.generalService.obtenerTasaInteresActivoPorId(this.id).subscribe(dato => {
        this.tasaitact = dato;
      },error => console.log(error));
  }

  irAlaListaDeTasasInteres(){
    this.router.navigate(['/tasa-interes']);
    swal('Tasa actualizada',`La tasa ${this.tasaitact.valorut} ha sido actualizado con exito`, `success`);
  }

  OnSubmit(): void {
    this.generalService.actualizarTasaInteresActivo(this.id, this.tasaitact).subscribe(() => {
      this.irAlaListaDeTasasInteres();
    },error => console.log(error));
  }
}
