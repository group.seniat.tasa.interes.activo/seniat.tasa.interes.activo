import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultasPorIdComponent } from './consultas-por-id.component';

describe('ConsultasPorIdComponent', () => {
  let component: ConsultasPorIdComponent;
  let fixture: ComponentFixture<ConsultasPorIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsultasPorIdComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConsultasPorIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
