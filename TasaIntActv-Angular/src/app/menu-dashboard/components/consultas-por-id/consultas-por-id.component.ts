import { Component } from '@angular/core';
import { TasaI } from '../../modelo/tasa-i';
import { ActivatedRoute } from '@angular/router';
import { GeneralService } from '../../lista_de_servicio/general.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-consultas-por-id',
  templateUrl: './consultas-por-id.component.html',
  styleUrl: './consultas-por-id.component.css'
})
export class ConsultasPorIdComponent {
  title = 'Consultas Por Id';
  id:number;
  tasaitact:TasaI;
  constructor(private route:ActivatedRoute, private generalService:GeneralService){} 

ngOnit(): void {
  this.id = this.route.snapshot.params['id'];
  this.tasaitact = new TasaI();
  this.generalService.obtenerTasaInteresActivoPorId(this.id).subscribe(dato => {
    this.tasaitact = dato;
    swal(`Consultas por id de la tasa ${this.tasaitact.valorut}`);
  });
}
}
