import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTasaInteresActivosComponent } from './lista-tasa-interes-activos.component';

describe('ListaTasaInteresActivosComponent', () => {
  let component: ListaTasaInteresActivosComponent;
  let fixture: ComponentFixture<ListaTasaInteresActivosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaTasaInteresActivosComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListaTasaInteresActivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
