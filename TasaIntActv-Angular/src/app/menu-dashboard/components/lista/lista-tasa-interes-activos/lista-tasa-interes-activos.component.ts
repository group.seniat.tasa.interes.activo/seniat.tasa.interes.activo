import { Component, OnInit } from '@angular/core';
import { TasaI } from '../../../modelo/tasa-i';
import { GeneralService } from '../../../lista_de_servicio/general.service';


@Component({
  selector: 'app-lista-tasa-interes-activos',
  templateUrl: './lista-tasa-interes-activos.component.html',
  styleUrl: './lista-tasa-interes-activos.component.css'
})
export class ListaTasaInteresActivosComponent implements OnInit{

  title = 'Lista-Tasa-Interes-Activos';

  tasaitacts: TasaI[];
  

  constructor (private generalService: GeneralService) { }

  mostrarComponente: string = ''; 
  ngOnInit(): void {
    this.obtenerTasaI();    
  }

  private obtenerTasaI() {
    this.generalService.obtenerListaDeTasasInteres().subscribe(dato => {
      this.tasaitacts = dato;
    });
  }
}

