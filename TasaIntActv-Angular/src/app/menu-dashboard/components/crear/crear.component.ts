import { Component, OnInit } from '@angular/core';
import { TasaI } from '../../modelo/tasa-i';
import { Router } from '@angular/router';
import { GeneralService } from '../../lista_de_servicio/general.service';
import swal from 'sweetalert';


@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css'],
})
export class CrearComponent implements OnInit {

  tasaitact: TasaI = new TasaI(); // Variable para la tasa que se creará
  title = 'Ingreso de de formulario TasaI';
  constructor(private generalService: GeneralService, private router: Router) {}

 ngOnInit(): void {
 }

 guardarTasaI(){
  this.generalService.crearTasaInteresActivo(this.tasaitact).subscribe(dato =>{
    console.log(dato);
    this.irAlaListaDeTasasInteres();
  },error=> console.log(error));
 }

 irAlaListaDeTasasInteres(){
  this.router.navigate(['/tasa-interes']);
  swal('Tasa registrado', `La tasa ${this.tasaitact.valorut} ha sido registrado con exito`, `success`)
 }

 onSubmit(){
  this.guardarTasaI();
    }
 }