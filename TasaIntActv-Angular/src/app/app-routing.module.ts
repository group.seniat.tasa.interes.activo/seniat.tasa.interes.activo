import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { FormsModule } from '@angular/forms';
import { CrearComponent } from './menu-dashboard/components/crear/crear.component';
import { MenuDashboardComponent } from './menu-dashboard/menu-dashboard.component';
import { ActualizarComponent } from './menu-dashboard/components/actualizar/actualizar.component';
import { EliminarComponent } from './menu-dashboard/components/eliminar/eliminar.component';
import { ListaTasaInteresActivosComponent } from './menu-dashboard/components/lista/lista-tasa-interes-activos/lista-tasa-interes-activos.component';
import { ConsultasPorIdComponent } from './menu-dashboard/components/consultas-por-id/consultas-por-id.component';



const routes: Routes = [
 
  { path: '', redirectTo: 'menu-dashboard', pathMatch: 'full' },
  { path: 'menu-dashboard', component: MenuDashboardComponent },  
  { path: 'crear', component: CrearComponent },
  { path: 'tasa-interes', component: ListaTasaInteresActivosComponent},
  {path: 'consultas-por-id', component: ConsultasPorIdComponent},
  { path: 'actualizar', component: ActualizarComponent},
  { path: 'eliminar', component: EliminarComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes),FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}